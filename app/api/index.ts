import type { Board, Catalog, Thread, ThreadAndReplies, ThreadList } from "~/api/models";

export const getBoards = async (): Promise<Board[]> => {
  const response = await fetch("https://a.4cdn.org/boards.json",
    {
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
      },
    });

  const json = await response.json();

  if (!json.boards) {
    return [] as Board[];
  }

  return json.boards;
};

export const getBoardCatalog = async (boardID: string): Promise<Catalog[]> => {
  const response = await fetch(`https://a.4cdn.org/${boardID}/catalog.json`,
    {
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
      },
    });

  return await response.json();
};

export const getThreadsList = async (boardID: string): Promise<ThreadList[]> => {
  const response = await fetch(`https://a.4cdn.org/${boardID}/threads.json`,
    {
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
      },
    });

  return await response.json();
};

export const getThreadIndex = async (boardID: string, page: number | string) => {
  const response = await fetch(`https://a.4cdn.org/${boardID}/${page}.json`,
    {
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
      },
    });

  return await response.json();
};

export const getThread = async (boardID: string, threadID: string): Promise<ThreadAndReplies> => {
  const response = await fetch(`https://a.4cdn.org/${boardID}/thread/${threadID}.json`,
    {
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
      },
    });

  return await response.json();
};

export const getPopularThreads = async (boards?: Board[]): Promise<Thread[]> => {
  if (!boards) {
    boards = await getBoards();
  }

  const _candidatesOfBoards: Thread[] = [];

  for (const board of boards) {
    const threadList = await getBoardCatalog(board.board);
    const _candidatesOfBoard: Thread[] = [];
    threadList.forEach(list => {
      _candidatesOfBoard.push(
        list.threads.reduce((t1, t2) => t1.replies > t2.replies ? t1 : t2),
      );
    });

    _candidatesOfBoards.push(
      _candidatesOfBoard.reduce((t1, t2) => t1.replies > t2.replies ? t1 : t2),
    );
  }

  return _candidatesOfBoards;
};
