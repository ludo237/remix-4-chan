// https://github.com/4chan/4chan-API/blob/master/pages/Boards.md#boardsjson-attributes=
export interface Board {
  board: string,
  title: string,
  ws_board: boolean, // 1 or 0
  per_page: number,
  pages: number,
  max_filesize: number,
  max_web_filesize: number,
  max_comment_chars: number,
  max_web_duration: number,
  bump_limit: number,
  image_limit: number,
  cooldowns: any[],
  meta_description: string,
  spoilers: boolean, // 1 or 0
  custom_spoilers: number,
  is_archived: boolean, // 1 or 0
  board_flags?: any[],
  country_flags: boolean, // 1 or 0
  user_ids: boolean, // 1 or 0
  oekaki: boolean, // 1 or 0
  sjis_tags: boolean, // 1 or 0
  code_tags: boolean, // 1 or 0
  math_tags: boolean, // 1 or 0
  text_only: boolean, // 1 or 0
  forced_anon: boolean, // 1 or 0
  web_audio: boolean, // 1 or 0
  require_subject: boolean, // 1 or 0
  min_image_width?: number,
  min_image_height?: number,
}

// https://github.com/4chan/4chan-API/blob/master/pages/Catalog.md#catalog-json-attributes=
export interface Catalog {
  page: number,
  threads: Thread[],
}

// https://github.com/4chan/4chan-API/blob/master/pages/Threads.md#thread-json-structure=
export interface Thread {
  no: number,
  resto: number,
  sticky: boolean,
  closed: boolean,
  now: string, // This is a date
  time: number,
  name: string,
  trip: string,
  capcode?: "mod" | "admin" | "admin_highlight" | "manager" | "developer" | "founder",
  country: string,
  country_name: string,
  board_flag: string,
  flag_name: string,
  sub: string,
  com: string,
  tim: number,
  filename: string,
  ext: ".jpg" | ".png" | ".gif" | ".pdf" | ".swf" | ".webm"
  fsize: number,
  md5: string,
  w: number,
  h: number,
  tn_w: number,
  tn_h: number,
  filedeleted: boolean,
  spoiler: boolean,
  custom_spoiler?: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10
  replies: number,
  images: number,
  bumplimit: boolean,
  imagelimit: boolean,
  tag?: string,
  semantic_url: string,
  since4pass: number,
  unique_ips: number,
  m_img: boolean,
  archived: boolean,
  archived_on: number,
}

export interface ThreadAndReplies {
  posts: Thread[];
}

export interface ThreadList {
  page: number,
  threads: Thread[],
}
