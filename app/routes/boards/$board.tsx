import { ChatAlt2Icon } from "@heroicons/react/outline";
import { PhotographIcon } from "@heroicons/react/solid";
import type { LoaderFunction } from "@remix-run/node";
import { json } from "@remix-run/node";
import { Link, useLoaderData } from "@remix-run/react";
import { formatDistanceToNow, fromUnixTime } from "date-fns";
import invariant from "tiny-invariant";
import { getBoardCatalog } from "~/api";
import type { Catalog } from "~/api/models";
import Card from "~/components/card/Card";
import CardBody from "~/components/card/CardBody";
import CardHeader from "~/components/card/CardHeader";

export const loader: LoaderFunction = async ({ params, request }) => {
  invariant(params.board, "Board ID is required");
  const url = new URL(request.url);
  const page = url.searchParams.get("page") || 1;
  const index = await getBoardCatalog(params.board);

  return json({
    board: params.board,
    catalog: index.filter(i => i.page === page)[0],
  });
};

export default function Board () {
  const { board, catalog } = useLoaderData<{ board: string, catalog: Catalog }>();

  return (
    <ul className={"space-y-6"}>
      {catalog.threads.map(t =>
        <li key={t.no}>
          <Card>
            <CardHeader
              color={"orange"}
              text={`${t.sub} ${formatDistanceToNow(fromUnixTime(t.time), { addSuffix: true })}`}
            />
            <CardBody>
              <div className={"flex flex-row space-x-3"}>
                <div className={"border-dotted border-2"}>
                  <img
                    data-width={t.w}
                    data-height={t.h}
                    alt={"img"}
                    src={`https://i.4cdn.org/${board}/${t.tim}${t.ext}`} data-md5={t.md5}
                  />
                </div>
                <div className={"prose prose-slate prose-sm"} dangerouslySetInnerHTML={{ __html: t.com }} />
              </div>
              <div className={"w-full text-sm"}>
                <Link to={`/boards/${board}/${t.no}`}>
                  <div className={"flex justify-end space-x-3"}>
                    <div className={"inline-flex justify-between items-center space-x-0.5 group"}>
                      <span className={"text-rose-400"}>{t.images}</span>
                      <PhotographIcon className={"h-5 w-5 text-rose-400 group-hover:text-rose-600"} />
                    </div>

                    <div className={"inline-flex justify-between items-center space-x-0.5 group"}>
                      <span className={"text-rose-400"}>{t.replies}</span>
                      <ChatAlt2Icon className={"h-5 w-5 text-rose-400 group-hover:text-rose-600"} />
                    </div>
                  </div>
                </Link>
              </div>
            </CardBody>
          </Card>
        </li>,
      )}
    </ul>
  );
}
