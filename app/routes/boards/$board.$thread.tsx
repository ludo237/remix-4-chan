import type { LoaderFunction } from "@remix-run/node";
import { json } from "@remix-run/node";
import { useLoaderData } from "@remix-run/react";
import { formatDistanceToNow, fromUnixTime } from "date-fns";
import invariant from "tiny-invariant";
import { getThread } from "~/api";
import type { Thread } from "~/api/models";
import Card from "~/components/card/Card";
import CardBody from "~/components/card/CardBody";
import CardHeader from "~/components/card/CardHeader";

export const loader: LoaderFunction = async ({ params }) => {
  invariant(params.board, "Board ID is required");
  invariant(params.thread, "Thread ID is required");

  const thread = await getThread(params.board, params.thread);

  return json({
    board: params.board,
    threads: thread.posts,
  });
};

export default function SingleThread () {
  const { board, threads } = useLoaderData<{ board: string, threads: Thread[] }>();
  const op = threads[0];

  return (
    <>
      <Card>
        <CardHeader
          color={"orange"}
          text={`${op.sub} ${formatDistanceToNow(fromUnixTime(op.time), { addSuffix: true })}`}
        />
        <CardBody>
          <div className={"prose prose-slate prose-sm"} dangerouslySetInnerHTML={{ __html: op.com }} />
        </CardBody>
      </Card>

      {threads.slice(1).map((t, index) =>
        <div key={index} className={"grid grid-cols-12"}>
          <div className={"col-span-1 grid grid-col-2 grid-flow-col grid-rows-2"}>
            <div />
            <div />
            <div className={"border-l border-b border-rose-800"} />
            {index === threads.length - 2 ? <div /> : <div className={"border-l border-rose-800"} />}
          </div>
          <div className={"col-span-11 py-6"}>
            <Card>
              <CardHeader
                color={"orange"}
                text={`${t.name} ${t.no} ${formatDistanceToNow(fromUnixTime(t.time), { addSuffix: true })}`}
              />
              <CardBody>
                <div className={"flex flex-row space-x-3"}>
                  <div className={"border-dotted border-2"}>
                    <img
                      data-width={t.w}
                      data-height={t.h}
                      alt={"img"}
                      src={`https://i.4cdn.org/${board}/${t.tim}${t.ext}`} data-md5={t.md5}
                    />
                  </div>
                  <div className={"prose prose-slate prose-sm"} dangerouslySetInnerHTML={{ __html: t.com }} />
                </div>
              </CardBody>
            </Card>
          </div>
        </div>,
      )}
    </>
  );
}
