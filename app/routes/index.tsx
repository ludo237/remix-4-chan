import { FireIcon } from "@heroicons/react/solid";
import type { LoaderFunction } from "@remix-run/node";
import { json } from "@remix-run/node";
import { Link, useLoaderData } from "@remix-run/react";
import { getBoards } from "~/api";
import type { Board } from "~/api/models";
import Card from "~/components/card/Card";
import CardBody from "~/components/card/CardBody";
import CardHeader from "~/components/card/CardHeader";

export const loader: LoaderFunction = async () => {
  const boards = await getBoards();
  // We do the sorting here to avoid triggering a sort each index redrawn
  return json<{ boards: Board[] }>({
    boards: boards.sort((a, b) => a.title.localeCompare(b.title)),
  });
};

export default function Index () {
  const loaderData = useLoaderData<{ boards: Board[] }>();

  return (
    <main className={"max-w-2xl mx-auto pt-5"}>
      <div className={"w-full flex justify-center items-center"}>
        <img
          className={"hidden lg:block w-auto"}
          src={"/logo-full.png"}
          alt={"4Chan"}
        />
      </div>
      <div className={"space-y-16"}>
        <Card>
          <CardHeader color={"red"} text={"What is 4Chan?"} />
          <CardBody>
            <div className={"space-y-4"}>
              <p>
                4chan is a simple image-based bulletin board where anyone can post comments and share images. There are boards dedicated to a variety
                of
                topics, from Japanese animation and culture to videogames, music, and photography. Users do not need to register an account before
                participating in the community. Feel free to click on a board below that interests you and jump right in!
              </p>
              <p>
                Be sure to familiarize yourself with the Rules before posting, and read the FAQ if you wish to learn more about how to use the site.
              </p>
            </div>
          </CardBody>
        </Card>

        <Card>
          <CardHeader color={"orange"} text={"Boards"} />
          <CardBody>
            <div className={`grid grid-rows-[repeat(26,_minmax(0,_1fr))] grid-cols-3 gap-x-5 gap-y-1.5 grid-flow-col auto-cols-fr`}>
              {loaderData?.boards.map((board, index) =>
                <Link
                  key={index}
                  to={`/boards/${board.board}`}
                  title={board.title}
                  className={"group"}
                >
                  <div className={"inline-flex items-center space-x-2"}>
                  <span className={"text-rose-800 transition-colors duration-150 group-hover:underline group-hover:text-rose-700"}>
                    {board.title}
                  </span>
                    {board.ws_board ? null : <FireIcon className={"h-3.5 w-3.5 text-red-400"} />}
                  </div>
                </Link>,
              )}
            </div>
          </CardBody>
        </Card>

        <Card>
          <CardHeader color={"orange"} text={"Popular Threads"} />
          <CardBody>
            <p>Work in progress... the algorithm is there but it's too slow </p>
          </CardBody>
        </Card>
      </div>
    </main>
  );
}
