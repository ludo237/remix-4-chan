import type { LoaderFunction } from "@remix-run/node";
import { json } from "@remix-run/node";
import { Outlet, useLoaderData } from "@remix-run/react";
import { getBoards } from "~/api";
import type { Board } from "~/api/models";
import Navbar from "~/components/Navbar";

export const loader: LoaderFunction = async () => {
  const boards = await getBoards();

  return json<{ boards: Board[] }>({
    boards,
  });
};

export default function Boards () {
  const { boards } = useLoaderData();

  return (
    <main className={"mx-auto px-2 sm:px-4 lg:px-8"}>
      <Navbar boards={boards} />
      <div className={"my-2 w-full border-2 border-rose-50"} />
      <div className={"pb-10"}>
        <Outlet />
      </div>
    </main>
  );
}
