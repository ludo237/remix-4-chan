import { Link, NavLink } from "@remix-run/react";
import type { Board } from "~/api/models";

type NavbarProps = {
  boards: Board[];
}

const defaultNavLinkStyle = "text-rose-800 hover:border-b-2 text-sm font-medium hover:border-rose-900 hover:text-rose-900";
const activeNavLinkStyle = "text-rose-800 border-b-2 text-sm font-medium border-rose-900 hover:text-rose-900";

export default function Navbar (props: NavbarProps) {
  const { boards } = props;

  return (
    <nav>
      <div className={"flex items-center h-16"}>
        {/*TODO: Switch to src-set here*/}
        <Link to={"/"}>
          <img
            className={"block lg:hidden h-8 w-auto"}
            src={"/logo.png"}
            alt={"4Chan"}
          />
          <img
            className={"hidden lg:block h-8 w-auto"}
            src={"/logo-full.png"}
            alt={"4Chan"}
          />
        </Link>
      </div>
      <div className={"break-words space-x-3"}>
        {boards.map((b, index) =>
          <NavLink
            key={index}
            to={`/boards/${b.board}`}
            className={({ isActive }) => isActive ? activeNavLinkStyle : defaultNavLinkStyle}
          >
            {b.board}
          </NavLink>,
        )}
      </div>
    </nav>
  );
}
