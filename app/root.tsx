import type { ErrorBoundaryComponent, LinksFunction, MetaFunction } from "@remix-run/node";
import { Links, LiveReload, Meta, Outlet, Scripts, ScrollRestoration } from "@remix-run/react";
import React from "react";
import Card from "~/components/card/Card";
import CardBody from "~/components/card/CardBody";
import CardHeader from "~/components/card/CardHeader";
import styles from "./compiled.css";

export const meta: MetaFunction = () => ({
  charset: "utf-8",
  title: "Remix 4Chan",
  description: "4Chan if it was built with Remix",
  keywords: "4chan, remix, clone",
  viewport: "width=device-width,initial-scale=1",
});

export const links: LinksFunction = () => {
  return [
    { rel: "stylesheet", href: styles },
    { rel: "shortcut icon", href: "https://s.4cdn.org/image/favicon.ico" },
    { rel: "apple touch icon", href: "https://s.4cdn.org/image/apple-touch-icon-iphone.png" },
  ];
};

const RootDocument = ({ children }: { children: React.ReactNode }) => {
  return (
    <html lang="en">
    <head>
      <Meta />
      <Links />
    </head>
    <body className={"min-h-screen bg-gradient-to-b from-orange-200 via-amber-50 to-yellow-50"}>
    {children}
    </body>
    </html>
  );
};

export default function App () {
  return (
    <RootDocument>
      <Outlet />
      <ScrollRestoration />
      <Scripts />
      <LiveReload />
    </RootDocument>
  );
}

export const ErrorBoundary: ErrorBoundaryComponent = ({ error }) => {
  return (
    <RootDocument>
      <main className={"max-w-2xl mx-auto pt-10 "}>
        <Card>
          <CardHeader color={"red"} text={"Oops looks like something went wrong"} />
          <CardBody>
            <div className={"overflow-auto"}>
              <p>Name: {error.name}</p>
              <p>Message: {error.message}</p>
              <p>Stack trace:</p>
              <pre>
            {error.stack}
          </pre>
            </div>
          </CardBody>
        </Card>
      </main>
    </RootDocument>
  );
};
